.. index::
   pair: Julie Blanc ; Composer avec les technologies du web Genèses instrumentales collectives pour le développement d'une communauté de pratique de designers graphiques

.. _julie_blanc_2023_06_07:

===========================================================================================================================================================================================
2023-06-07 **PHD Julie Blanc Composer avec les technologies du web Genèses instrumentales collectives pour le développement d'une communauté de pratique de designers graphiques**
===========================================================================================================================================================================================

.. figure:: images/soutenance.png
   :align: center


Graphic designer, web utopist, 💜 CSS lover for web & print ⟡ I design publications 📖 and work on pagedjs.org / I write in french & english
===============================================================================================================================================

- https://julie-blanc.fr/#section-projects
- https://fosstodon.org/@julieblanc

.. figure:: images/julie_blanc.png
   :align: center

   https://julie-blanc.fr/

Liens
------

- https://www.instagram.com/julieblancfr/

Description
============

Thèse de doctorat en ergonomie et design, menée sous la direction de
Anne Bationo-Tillon, la codirection de Samuel Bianchini et le coencadrement
de Lucile Haute, soutenue par l'EUR ArTeC et préparée à l'université Paris 8
au sein du laboratoire EA 349 Paragraphe (école doctorale 224 Cognition,
Langage, Interaction), équipe C3U et à l'École nationale supérieure des
Arts Décoratifs - PSL, au sein d'EnsadLab, équipe Reflective Interaction.

La thèse est accompagnée d'une exposition du 5 au 9 juin 2023 à la
bibliothèque du Musée des Arts Décoratifs.


Résumé
============

Depuis une dizaine d’années, des designers graphiques développent de
nouvelles pratiques autour de l’utilisation des technologies du web pour
la mise en forme de publications imprimées et multisupports.

Situées dans la culture du logiciel libre et de l’open-source, ces démarches
sont au cœur d’enjeux conceptuels, politiques et esthétiques pour le
design graphique.

Comment comprendre alors ce que l’utilisation réelle du code change dans
la pratique des designers graphiques et en quoi ce changement technique
participe-t-il à la construction et au développement d’une communauté
de pratique ?

Nous inscrivons notre recherche dans une compréhension anthropocentrée
de la technique.

Pour cela, nous utilisons la théorie historico-culturelle de l’activité
et l’approche instrumentale pour étudier l’activité de composition et
son développement ainsi que les changements du système socio-technique
associé.

De plus, dans une perspective de recherche portée par la pratique, nous
mobilisons notre propre expérience de designer graphique inscrite dans
une communauté d’acteurs et d’actrices porteurs de ces transformations.

Trois études empiriques analysent l’activité de composition sous
plusieurs angles.
L’une d’elle participe, sous la forme d’un hackathon, à la conception
collective d’un outil permettant la création de livres imprimés depuis
les navigateurs web (paged.js).

Nos résultats sont mis en perspective avec des questionnements plus
généraux des transformations sociotechniques induites par l’introduction
des pratiques du logiciel libre dans le design graphique.

Nous soutenons que l’introduction des technologies du web dans la chaîne
graphique accompagnent les pratiques créatives et esthétiques des designers
et ouvrent de nouvelles perspectives d’activité collective, permettant
à cette communauté de développer ses propres instruments et donc ses
propres pratiques.


2023-06-02 👩‍🎓 **Je suis très heureuse de vous annoncer ma soutenance de thèse**
=======================================================================================

- https://fosstodon.org/@julieblanc/110473282388155008

👩‍🎓Je suis très heureuse de vous annoncer ma soutenance de thèse, qui aura
lieu le 7 juin à 13h au MAD Paris (réservation obligatoire).

Elle sera accompagnée d’une exposition visible du 5 au 9 juin, avec pleins
de livres en #cssprint (#pagedjs).

Plus d’informations ➡️ http://phd.julie-blanc.fr

bortzmeyer@mastodon.gougere.fr
==============================

bortzmeyer@mastodon.gougere.fr - Un exemple de bibliothèque citée par l'auteure : http://web.2print.org/


.. figure:: images/css_trop_bien.png
   :align: center

