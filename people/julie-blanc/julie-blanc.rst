.. index::
   pair: Julie Blanc; People
   ! Julie Blanc

.. _julie_blanc:

=====================================
**Julie Blanc**
=====================================

- https://julie-blanc.fr/
- https://fosstodon.org/@julieblanc
- https://nitter.caioalonso.com/julieblancfr/rss
- https://fosstodon.org/@julieblanc
- https://julie-blanc.fr/
- https://gitlab.com/JulieBlanc
- https://github.com/JulieBlanc
- https://phd.julie-blanc.fr/
- https://phd.julie-blanc.fr/expo.html
- https://pagedjs.org/
- https://www.instagram.com/designenrecherche/
- https://prepostprint.org/resources/


.. toctree::
   :maxdepth: 4

   2023/2023
