.. index::
   ! Cascading Style Sheets
   ! CSS

.. raw:: html

   <a rel="me" href="https://framapiaf.org/@pvergain"></a>
   <a rel="me" href="https://babka.social/@pvergain"></a>

|FluxWeb| `RSS <https://gdevops.frama.io/web/tuto-css/rss.xml>`_


.. _css:

=====================================
**Tuto CSS**
=====================================

- https://developer.mozilla.org/en-US/docs/Web/CSS
- https://en.wikipedia.org/wiki/Cascading_Style_Sheets
- https://developer.mozilla.org/en-US/docs/Learn/CSS
- https://caniuse.com/
- https://www.30secondsofcode.org/css/p/1/


.. figure:: images/CSS3_logo_and_wordmark.svg.png
   :align: center


.. figure:: images/css-isnt-easy.png
   :align: center

   https://wizardzines.com/comics/css-isnt-easy/

.. toctree::
   :maxdepth: 6

   news/news
   definition/definition
   people/people
   animations/animations
   borders/borders
   frameworks/frameworks
   tutorials/tutorials
