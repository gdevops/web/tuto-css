
.. index::
   pair: CSS ; tailwincss

.. _tailwindcss:

============================================================================================
**tailwincss A utility-first CSS framework for rapidly building custom user interfaces**
============================================================================================


- https://tailwindcss.com/
- https://github.com/tailwindlabs/tailwindcss
- https://github.com/tailwindlabs/tailwindcss/discussions
- https://tailwindcss.com/discord
- https://www.youtube.com/c/TailwindLabs
- https://play.tailwindcss.com/

.. toctree::
   :maxdepth: 3

   versions/versions
