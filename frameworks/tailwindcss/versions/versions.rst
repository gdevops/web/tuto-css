
.. index::
   pair: Versions ; tailwincss

.. _tailwindcss_versions:

=====================================
tailwincss versions
=====================================

- https://tailwindcss.com/
- https://github.com/tailwindlabs/tailwindcss
- https://tailwindcss.com/discord
- https://www.youtube.com/c/TailwindLabs


.. toctree::
   :maxdepth: 3

   3.1.0/3.1.0
   3.0.0/3.0.0
