

.. _tailwindcss_3_0_0:

=====================================
tailwindcss **3.0.0 (2021-12-09)**
=====================================


- https://github.com/tailwindlabs/tailwindcss/releases/tag/v3.0.0
- https://www.youtube.com/watch?v=TmWIrBPE6Bc (Introducing Tailwind CSS v3.0)
