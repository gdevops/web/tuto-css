
.. index::
   pair: Themes ; bootswatch

.. _bootswatch:

=====================================
bootswatch
=====================================

.. seealso::

   - https://github.com/thomaspark/bootswatch
   - https://bootswatch.com/
   - https://x.com/bootswatch
   - https://x.com/thomashpark

.. toctree::
   :maxdepth: 3

   versions/versions
