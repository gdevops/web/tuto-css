
.. index::
   pair: CSS ; Bootstrap

.. _css_bootstrap:

=====================================
**bootstrap**
=====================================

- https://en.wikipedia.org/wiki/Bootstrap\_(front-end_framework)
- https://x.com/mdo
- https://x.com/getbootstrap
- https://x.com/SBootstrap
- https://github.com/twbs/bootstrap
- https://getbootstrap.com/
- https://blog.getbootstrap.com/
- https://github.com/twbs/bootstrap/discussions

.. figure:: Boostrap_logo.svg.png
   :align: center


.. toctree::
   :maxdepth: 3

   definition/definition
   history/history
   themes/themes
   examples/examples
   versions/versions
