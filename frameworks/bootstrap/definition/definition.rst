
.. index::
   pair: Definition ; Bootstrap

.. _css_bootstrap_def:

=====================================
bootstrap definition
=====================================



English wikipedia definition
==============================

.. seealso::

   - https://en.wikipedia.org/wiki/Bootstrap_(front-end_framework)


Bootstrap is a free and open-source CSS framework directed at responsive,
mobile-first front-end web development.

It contains CSS- and (optionally) JavaScript-based design templates for
typography, forms, buttons, navigation and other interface components.

Bootstrap is the third-most-starred project on GitHub, with more than 131,000 stars,
behind only freeCodeCamp (almost 300,000 stars) and marginally behind Vue.js
framework.

According to Alexa Rank, Bootstrap getbootstrap.com is in the top-2000 in US
while vuejs.org is in top-7000 in US.

French wikipedia definition
==============================

.. seealso::

   - https://fr.wikipedia.org/wiki/Bootstrap_(front-end_framework)

Bootstrap est une collection d'outils utiles à la création du design
(graphisme, animation et interactions avec la page dans le navigateur, etc.)
de sites et d'applications web.

C'est un ensemble qui contient des codes HTML et CSS, des formulaires, boutons,
outils de navigation et autres éléments interactifs, ainsi que des extensions
JavaScript en option.

C'est l'un des projets les plus populaires sur la plate-forme de gestion de
développement GitHub.
