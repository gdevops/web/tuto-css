

.. _css_bootstrap_5_0_def:

=======================================
bootstrap 5.0 definition
=======================================

.. seealso::

   - https://github.com/twbs/bootstrap/releases/
   - https://en.wikipedia.org/wiki/Bootstrap_(front-end_framework)#Bootstrap_5
   - https://blog.getbootstrap.com/2018/12/21/bootstrap-4-2-1/


Wikipedia Announce
===================

.. seealso::

   - https://en.wikipedia.org/wiki/Bootstrap_(front-end_framework)#Bootstrap_5

Bootstrap 5 is the upcoming major version of the framework.

Major changes include:

- Switch from jQuery library to native JavaScript
- Dropping support for IE10
- Moving testing infrastructure from QUnit to Jasmine

Announce 2018-12-21
=====================

.. seealso::

   - https://blog.getbootstrap.com/2018/12/21/bootstrap-4-2-1/

What’s next

We have v4.3 already planned, so that’s our immediate focus.

However, while we’re developing that in the v4-dev branch, we’ll be getting our
plans in order for a v5 release.

**Bootstrap 5 will not feature drastic changes to the codebase.**

While I tweeted about the earnestness to move to PostCSS years ago, we’ll still
be on Sass for v5. Instead, we’ll focus our efforts on removing cruft, improving
existing components, and dropping old browsers and our jQuery dependency.

There are also some updates to our v4.x components we cannot make without
causing breaking changes, so v5 feels like it’s coming at the right time for us.

**Stay tuned for a preview of the plans for v5 in the new year**.

We’ll share via  an issue, ask for feedback, and then settle in to development mode.

Happy holidays, and happy new year to everyone !

Thanks for continuing to make Bootstrap an amazing project and community.
