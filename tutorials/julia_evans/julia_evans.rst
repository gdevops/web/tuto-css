

.. _css_julia_evans:

=====================================
CSS **Julia evans**
=====================================

.. seealso::

   - https://x.com/b0rk
   - https://wizardzines.com/comics/
   - https://x.com/b0rk/status/1305875761782575105?s=20




inline-vs-block
==================

.. seealso::

   - https://wizardzines.com/comics/inline-vs-block/


.. figure:: inline-vs-block.png
   :align: center


CSS isn't arbitrary
======================

.. seealso::

   - https://x.com/b0rk/status/1305875761782575105?s=20
