.. index::
   pair: csslayout.io; CSS

.. _csslayout_io:

==============================================================================
**csslayout.io** A collection of popular layouts and patterns made with CSS
==============================================================================

- https://csslayout.io/
- https://github.com/phuoc-ng/csslayout
- https://github.com/phuoc-ng/csslayout/commits/master

.. literalinclude:: README.md
