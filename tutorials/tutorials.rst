.. index::
   pair: CSS ; Tutorials

.. _css_tutos:

=====================================
CSS tutorials
=====================================

.. toctree::
   :maxdepth: 6

   30secondsofcode/30secondsofcode
   csslayout.io/csslayout.io
   julia_evans/julia_evans
   mozilla/mozilla
   w3schools/w3schools
