

.. _css_30secondsofcode:

=====================================
**30secondsofcode**
=====================================

.. seealso::

   - https://www.30secondsofcode.org/css/p/1/
   - https://x.com/30secondsofcode
   - https://github.com/30-seconds
   - https://github.com/30-seconds/30-seconds-of-css

.. toctree::
   :maxdepth: 3

   animation/animation
   interactivity/interactivity
   layout/layout
   visual/visual
