.. index::
   pair: Firefox 128 ;  @property
   ! @property

.. _at_property_2024_07_19:

======================================================================================
2024-07-19 **Providing Type Definitions for CSS with @property (Firefox 128)**
======================================================================================

- https://moderncss.dev/providing-type-definitions-for-css-with-at-property/


A cross-browser feature as of the release of Firefox 128 in July 2024 is 
a new at-rule - @property - which allows defining types as well as 
inheritance and an initial value for your custom properties.

We'll learn when and why traditional fallback values can fail, and how 
@property features allow us to write safer, more resilient CSS custom 
property definitions.
