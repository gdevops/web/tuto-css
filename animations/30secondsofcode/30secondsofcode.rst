
.. _30secondsofcode_css_animations:

=====================================
30secondsofcode CSS animations
=====================================

- https://www.30secondsofcode.org/css/t/animation/p/1
- https://github.com/30-seconds/30-seconds-of-css

Introduction
============

The CSS snippet collection contains utilities and interactive examples
for CSS3.

It includes modern techniques for creating commonly-used layouts,
styling and animating elements, as well as snippets for handling user
interactions.


