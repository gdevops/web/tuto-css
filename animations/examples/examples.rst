
.. _css_animations_examples:

=====================================
CSS animations examples
=====================================


.. toctree::
   :maxdepth: 3

   bouncing_loader/bouncing_loader
   pulse_loader/pulse_loader
