.. index::
   pair: CSS ; animations
   ! Animations

.. _css_animations:

=====================================
CSS animations
=====================================

- https://javascript.info/css-animations
- https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Transitions/Using_CSS_transitions
- https://htmx.org/docs/#indicators
- http://samherbert.net/svg-loaders/
- https://github.com/SamHerbert/SVG-Loaders
- https://www.freecodecamp.org/news/css-transition-examples/
- https://www.30secondsofcode.org/css/t/animation/p/1

.. toctree::
   :maxdepth: 3

   javascript_info/javascript_info
   30secondsofcode/30secondsofcode
   examples/examples
